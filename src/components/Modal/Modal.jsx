import { Component, createRef } from 'react';
import PropTypes from 'prop-types'

import { ReactComponent as Closed } from './icons/closed.svg';

import styled from 'styled-components';

const ModalWindow = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: 100vh;
width: 100vw;
background-color: rgba(0,0,0,.3);
position: fixed;
top: 0;
left: 0;
`;

const ModalBody = styled.div`
background-color: ${props => props.backgroundColor || 'lightgray'};
border-radius: 5px;
height: 250px;
width: 550px;
`;

const Modalheader = styled.div`
display: flex;
align-items: baseline;
justify-content: space-between;
background-color: rgba(0,0,0,.2);
padding: 20px;
`;

const CloseButton = styled.button`
background-color: transparent;
border: none;
cursor: pointer;
`;

const ModalTitle = styled.h2`
padding: 0 30px 0 0;
text-align: center;
font-size: 24px;
color:  ${props => props.color || 'black'};
`;

const ModalText = styled.p`
text-align: center;
font-size: 16px;
padding: 20px;
color:  ${props => props.color || 'black'};
`;

const ModalFooter = styled.div`
text-align: center;
font-size: 16px;
padding: 10px;
`;

class Modal extends Component {

	constructor() {
		super();

		this.wrapperRef = createRef();
		this.handleClickOutside = this.handleClickOutside.bind(this);
	}

	componentDidMount() {
		document.addEventListener("mousedown", this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener("mousedown", this.handleClickOutside);
	}



	handleClickOutside(event) {
		if (this.wrapperRef && !this.wrapperRef.current.contains(event.target))
			this.props.closeButton()
	}


	render() {
		const { closeButton, actions, text, header } = this.props;

		return (
			<ModalWindow >
				<ModalBody ref={this.wrapperRef} {...this.props}>
					<Modalheader>
						<ModalTitle {...this.props}>
							{header}
						</ModalTitle>
						<CloseButton onClick={closeButton}>
							<Closed />
						</CloseButton>
					</Modalheader>
					<ModalText {...this.props}>
						{text}
					</ModalText>
					<ModalFooter />
					{actions}
				</ModalBody>
			</ModalWindow>
		)
	}
}

Modal.propTypes = {
	closeButton: PropTypes.func,
	actions: PropTypes.object,
	text: PropTypes.string,
	header: PropTypes.string
}

export default Modal;
