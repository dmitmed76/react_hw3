import PropTypes from 'prop-types';

import {
	BasketCardTitle,
	BasketGoodsColor,
	BasketCardArt,
	BasketCardImg,
	BasketCardPrice,
	BasketCardsWrapper,
	DeleteButton,
	Icons
} from './styledBasket'

export default function Basket({ basket, deleteOrder, openModalTwo, hendlerCurrentGoods }) {


	return (

		<>
			{basket.map((el) => (
				<BasketCardsWrapper key={el.article} >
					<BasketCardImg src={el.url} alt={el.title} />
					<BasketCardTitle>{el.title}</BasketCardTitle>
					<BasketGoodsColor>Колір: {el.color}</BasketGoodsColor>
					<BasketCardArt>Арт.: {el.article}</BasketCardArt>
					<BasketCardPrice>Ціна: {el.price} грн.</BasketCardPrice>
					<DeleteButton type="button" onClick={() => {
						deleteOrder(el.article)
						openModalTwo()
						hendlerCurrentGoods(el)
					}} >
						<Icons />
					</DeleteButton>
				</BasketCardsWrapper>
			)
			)}
		</>
	)
}

Basket.propTypes = {
	basket: PropTypes.array,
}
