import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from "react-router-dom";

import { Global } from './styledIndex'
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));


root.render(
	<React.StrictMode>
		<Global />
		<BrowserRouter>
			<App />
		</BrowserRouter>


	</React.StrictMode>
);
