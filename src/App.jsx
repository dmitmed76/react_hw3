import { useState, useEffect } from 'react'
import { Routes, Route } from 'react-router-dom';

import { AppWrapper, ButtonModalWrapper, ButtonModal, MenuWrapper, LinkCustom } from './styledApp';
import { Modal } from './components/Modal';
import { Header } from './components/Header';
import { Home } from './pages/Home';
import { Favorites } from './pages/Favorites';
import { Basket } from './pages/Basket';


function App() {
	const [isModal, setIsModal] = useState(false);
	const [isModalTwo, setIsModalTwo] = useState(false);
	const [projectors, setProjectors] = useState([]);
	const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || []);
	const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
	const [currentGoods, setCurrentGoods] = useState({});
	const [goods, setGoods] = useState({});

	localStorage.setItem('basket', JSON.stringify(basket));
	localStorage.setItem('favorites', JSON.stringify(favorites));

	useEffect(() => {
		fetch("goods.json")
			.then(res => res.json())
			.then(
				(result) => {
					setProjectors(JSON.parse(localStorage.getItem('favoritIcon')) || result)
				}
			)
	}, []);

	const toggleIcons = (pos) => {
		console.log('!_!',  pos);
		const temp = [...projectors];
		const result = temp.find(({ article }) => article === pos);
		result.isFavorite = !result.isFavorite;
		console.log('!_!', result.isFavorite,temp);
		localStorage.setItem('favoritIcon', JSON.stringify(temp));
		setProjectors(temp);
	}

	const hendlerModal = () => {
		setIsModal(!isModal);

	}

	const hendlerModalTwo = () => {
		setIsModalTwo(!isModalTwo);

	}

	const hendlerCurrentGoods = (currentGoods) => {
		setCurrentGoods({ ...currentGoods });
	}

	const hendlerGoods = (goods) => {
		console.log(goods);
		setGoods({ ...goods });
	}

	const hendlerFavorites = (goods) => {
		setFavorites([...favorites, goods]);
		hendlerGoods(goods)

	}

	const deleteFavorites = (goods) => {
		setFavorites(favorites.filter(el => el.article !== goods.article))
	}

	const changeFavorite = (goods) => {
		console.log('gooods',goods.article);
		const res = favorites.some((el) => goods.article === el.article);
		if (res) {
			deleteFavorites(goods);
			toggleIcons(goods.article);
		} else {
			hendlerFavorites(goods);
		}
	}
	const hendlerBasket = (currentGoods) => {
		let isInArray = false;
		basket.forEach(el => {
			if (el.article === currentGoods.article)
				isInArray = true;
		});
		if (!isInArray)
			setBasket([...basket, currentGoods]);
		hendlerModal()
	}

	const deleteOrder = (currentGoods) => {
		setBasket(basket.filter(el => el.article !== currentGoods.article))
		hendlerModalTwo()
	}

	const actions = (
		<ButtonModalWrapper>
			<ButtonModal type='button'
				onClick={isModal ? () => hendlerBasket(currentGoods) : () => deleteOrder({ ...currentGoods })}>ok</ButtonModal>
			<ButtonModal type='button' backgroundColor={'#B43727'}
				onClick={isModal ? hendlerModal : hendlerModalTwo}>cancel</ButtonModal>
		</ButtonModalWrapper>
	)

	return (
		<AppWrapper>
			<Header
				countBasket={basket.length}
				countFavorite={favorites.length} />

			<MenuWrapper>
				<LinkCustom to="/">Home</LinkCustom>
				<LinkCustom to="/Favorites">Favorites</LinkCustom>
				<LinkCustom to="/Basket">Basket</LinkCustom>
			</MenuWrapper>

			<Routes>
				<Route path='/' element={<Home
					projectors={projectors}
					hendlerModal={hendlerModal}
					hendlerCurrentGoods={hendlerCurrentGoods}
					hendlerFavorites={hendlerFavorites}
					toggleIcons={toggleIcons}
					deleteFavorites={deleteFavorites}
				/>} />
				<Route path='/Favorites' element={<Favorites
					favorites={favorites}
					changeFavorite={changeFavorite}
				/>}
				/>
				<Route path='/Basket' element={<Basket
					basket={basket}
					deleteOrder={deleteOrder}
					openModalTwo={hendlerModalTwo}
					hendlerCurrentGoods={hendlerCurrentGoods}
				/>}
				/>
			</Routes>

			{isModal && <Modal
				actions={actions}
				closeButton={hendlerModal}
				header="Хочете додати цей товар в корзину?"
				text="Натисніть 'ОК' для додавання товару в корзину або натисніть 'CANCEL' для відміни "
				backgroundColor="lightblue"
				color="white"
			/>}


			{isModalTwo && <Modal
				actions={actions}
				closeButton={hendlerModalTwo}
				header='Ви дійсно хочете видалити цей товар з корзини?'
				text="Натисніть 'ОК' для іидалення товару з корзини або натисніть 'CANCEL' для відміни операції"
			/>}

		</AppWrapper >

	)
}


export default App;

